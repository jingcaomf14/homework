# README #

Name: Jing Cao  jc5270@nyu.edu

Instructions on reading the assignment

1. Java Programs are located in the MonteCarlo/src/main/java/hw3 folder

2. New Class Written for generating Normal Random Vectors is called BoxMuller.java

3. NormalVectorGenerator class is changed from previous assignment to use BoxMuller class.

4. Junit Test Files are located in the MonteCarlo/src/test/java/hw3 folder

5. Run the program through SimulationManager class, it will print out pricing values in progress.
After the price has converged, it will print out the final price and the total minutes it took to price the option.

6. GPU is utilized in generating in the BoxMuller class.