package hw3;

public interface PayOut {
	public double getPayout(StockPath path);
}
