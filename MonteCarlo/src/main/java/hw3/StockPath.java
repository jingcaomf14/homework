package hw3;
import java.util.List;
import org.joda.time.DateTime;


public interface StockPath {
	
	public List<Pair<DateTime, Double>> getPrice();
	
}
