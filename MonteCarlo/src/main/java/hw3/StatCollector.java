package hw3;

public class StatCollector {

	public  double payOutAverage;
	public  double payOutSquaredAverage;
	public  int n; // keeps track of number of gathered payout experiment already collected
	public  double sigma;
	
	public StatCollector()
	{
		this.payOutAverage = 0;
		this.payOutSquaredAverage = 0;
		this.n=0;
		this.sigma=0;
	}
	
	public int GetN()
	{
		return n;
	}
	
	public double GetPrice()
	{
		return payOutAverage;
	}
	
	public void Update(double newPayOut)
	{
		
		this.n++;
		this.payOutAverage = payOutAverage * (n-1)/n + newPayOut / n;
		this.payOutSquaredAverage = payOutSquaredAverage * (n-1)/n + newPayOut*newPayOut/n;
		this.sigma = Math.sqrt(payOutSquaredAverage - payOutAverage*payOutAverage);
		
	}
	
	public double Report()
	{
		return (double) this.sigma/Math.sqrt(this.n);
	}
	
}
