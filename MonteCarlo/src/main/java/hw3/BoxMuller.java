package hw3;
import java.util.Random;
import com.nativelibs4java.opencl.*;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.bridj.Pointer;
import static org.bridj.Pointer.allocateFloats;

/**
 * Created by jingcao on 12/10/14.
 * This BoxMuller class is used to generate 2 arrays of each 2 million uniform random float numbers and
 * transforming them to 2 normal random vectors
 */
public class BoxMuller {
    final int n = 2000000;
     Pointer<Float>
            aPtr = allocateFloats(n),
            bPtr = allocateFloats(n);


    public void Initialize( ) {

        // create uniform vectors
        UniformRealDistribution uniformSeed= new UniformRealDistribution();
        for (int i = 0; i < n; i++) {
            aPtr.set(i, (float) uniformSeed.sample());
            bPtr.set(i, (float) uniformSeed.sample());
        }
        // Creating the platform which is out computer.
        CLPlatform clPlatform = JavaCL.listPlatforms()[0];
        // Getting the GPU device
        CLDevice device = clPlatform.listGPUDevices(true)[0];
        //CLDevice device = clPlatform.listAllDevices(true)[0];
        // Verifing that we have the GPU device
        //System.out.println("*** New device *** ");
        //System.out.println("Vendor: " + device.getVendor());
        //System.out.println("Name: " + device.getName());
        //System.out.println("Type: " + device.getType());
        // Let's make a context
        CLContext context = JavaCL.createContext(null, device);
        // Lets make a default FIFO queue.
        CLQueue queue = context.createDefaultQueue();

        // Read the program sources and compile them :
        String src = "__kernel void normal_floats( __global float* a, " + "__global float* b, int n) \n" +
                "{\n" +
                "    int i = get_global_id(0);\n" +
                "    if (i >= n)\n" +
                "        return;\n" +
                "\n" +
                "    float out1 = sqrt(-2*log(a[i]))* cospi(2*b[i]);\n" +
                "    float out2 = sqrt(-2*log(a[i]))* sinpi(2*b[i]);\n" +
                "       a[i]=out1;\n" +
                "       b[i]=out2;\n" +
                "}";
        CLProgram program = context.createProgram(src);
        program.build();

        CLKernel kernel = program.createKernel("normal_floats");


        //System.out.println("New Batch of 2M Uniform Random Vector Generated");

        // Create OpenCL input buffers (using the native memory pointers aPtr and bPtr) :
        CLBuffer<Float>
                a = context.createFloatBuffer(CLMem.Usage.Input, aPtr),
                b = context.createFloatBuffer(CLMem.Usage.Input, bPtr);
        // Create OpenCL output buffers (using the native memory pointers cPtr and dPtr) :

        kernel.setArgs(a, b, n);

        CLEvent event = kernel.enqueueNDRange(queue, new int[]{n}, new int[]{128});
        event.invokeUponCompletion(new Runnable() {
            @Override
            public void run() {
            }

        });
         aPtr = a.read(queue,event);
         bPtr = b.read(queue,event);

        // System.out.println("New Batch of 2M Normal Random Vector Generated");
        //for (int i=0; i<110;i++)
        //{
         //  System.out.println(aPtr.get(i)+" "+bPtr.get(i)+"\n");
        //}
    }
}




