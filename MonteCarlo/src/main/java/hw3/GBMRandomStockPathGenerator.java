package hw3;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;


public class GBMRandomStockPathGenerator implements StockPath{
	private double rate;
	private double sigma;
	private double S0;
	private int N;
	private DateTime startDate;
	private DateTime endDate;
	private RandomVectorGenerator rvg;
	
	public GBMRandomStockPathGenerator(double rate, int N,
			double sigma, double S0,
			DateTime startDate, DateTime endDate,
			RandomVectorGenerator rvg){
		this.startDate = startDate;
		this.endDate = endDate;
		this.rate = rate; // rate is per day
		this.S0 = S0;
		this.sigma = sigma;
		this.N = N;
		this.rvg = rvg;
	}
	
	@Override
	// Note" the List returned is a LinkedList
	public List<Pair<DateTime, Double>> getPrice() {
		double[] n = rvg.getVector();
		DateTime current = new DateTime(startDate.getMillis());
		long delta = (endDate.getMillis() - startDate.getMillis())/N; // delta is the stepsize
		List<Pair<DateTime, Double>> path = new LinkedList<Pair<DateTime,Double>>();
		path.add(new Pair<DateTime, Double>(current, S0));
		for ( int i=1; i < N; ++i){
			current.plusMillis((int) delta);
			path.add(new Pair<DateTime, Double>(current, 
					path.get(path.size()-1).getValue()*Math.exp((rate-sigma*sigma/2)+sigma * n[i-1])));
		}
		return path;
	}
	
	
}
