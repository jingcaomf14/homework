package hw3;


import org.bridj.Pointer;

import static org.bridj.Pointer.allocateFloats;

// This class generates normal random vectors by picking normal variables from two pointers each of length 2 million
// once the numbers on the pointers are depleted, a new batch of normal random vectors are generated via
// the initialize method in the BoxMuller class.

public class NormalVectorGenerator implements RandomVectorGenerator {

	private int N;
	final int n = 2000000;
	// xPosition keeps track of current location on the 2 million long pointer
	static int xPosition;
	static BoxMuller GiantNormVec = new BoxMuller();


	public NormalVectorGenerator (int N){
		this.N=N;
		GiantNormVec.Initialize();
	}
	@Override

	public double[] getVector(){
		double[] vector = new double[N];

		//GiantNormVec.Initialize(GiantNormVec.cPtr,GiantNormVec.dPtr);
		//for (int i=0; i<110;i++)
		//{
		//	System.out.println(cPtr.get(i)+" "+dPtr.get(i)+"\n");
		//}
		for (int i = 0; i < vector.length; ++i)

		{
			if (xPosition<n) {
				//System.out.println(NormalVectorGenerator.xPosition);
				if (i%2==0)
				{vector[i] = GiantNormVec.aPtr.get(xPosition);}
				else
				{vector[i]= GiantNormVec.bPtr.get(xPosition);}
				xPosition++;
			}
			else
			{
				GiantNormVec.Initialize();
				this.xPosition=0;
				vector[i] = GiantNormVec.aPtr.get(xPosition);
				//System.out.println(vector[i]);
				xPosition++;
			}
		}
		//for (int i=0; i<N;i++)
		//{
		//	System.out.println(vector[i]);
		//}
		return vector;
	}
}
