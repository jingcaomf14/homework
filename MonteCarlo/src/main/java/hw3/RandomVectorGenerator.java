package hw3;

public interface RandomVectorGenerator {
	public double[] getVector();
}
