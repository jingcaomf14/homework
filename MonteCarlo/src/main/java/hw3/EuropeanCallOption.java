package hw3;

import java.util.List;

import org.joda.time.DateTime;

public class EuropeanCallOption implements PayOut {
	
	private double K; // K is the strike price
	
	public EuropeanCallOption(double K){
		this.K = K;
	}

	@Override
	public double getPayout(StockPath path) {
		List<Pair<DateTime,Double>> trajectory = path.getPrice();
		double[] prices = new double[trajectory.size()];
		for (int i=0; i<trajectory.size(); i++)
		{
			prices[i]=trajectory.get(i).getValue();
		}
		return Math.max(0, prices[prices.length-1] - K);
	}

}
