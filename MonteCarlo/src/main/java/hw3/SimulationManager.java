package hw3;

import org.joda.time.DateTime;
import org.apache.commons.math3.distribution.NormalDistribution;
public class SimulationManager {

	static double rate = 0.0001; // interest rate
	static double sigma= 0.01; // volatility of stock
	static int N=252; // number of time steps (one step a day)
	static DateTime startDate=DateTime.now();  // date that the option is valued which is now
	static DateTime endDate=startDate.plusDays(252); // date the option expires
	static double S0=152.35; // current stock price



	public static void main(String arg[]){
		// Parameters for precision control
		double confidenceProb =0.96;
		double errorAbsoluteTol = 0.01;
		NormalDistribution stdNormal = new NormalDistribution();
		double normalCriticalValue =stdNormal.inverseCumulativeProbability (1-(double)( 1-confidenceProb)/2);
		double hurdleValue = errorAbsoluteTol/normalCriticalValue;
		// hurdlevalue is boundary for sigma/sqrt(n), sigma is the standard error for payout in the n samples
		System.out.println("TARGET Status Hurdle Value is " +String.valueOf(hurdleValue));;


		// create a IBM GBMRandomStockPath generator

		RandomVectorGenerator Z = new AntiTheticVectorGenerator(new NormalVectorGenerator(N));
		// Z is a anti-thetic normal random vector of N dimensions generator
		GBMRandomStockPathGenerator IBMStockPath = new  GBMRandomStockPathGenerator(rate,  N,
				sigma,  S0, startDate, endDate, Z);

		// price IBM European option
		System.out.println("IBM European Option: Pricing....");
		EuropeanCallOption IBM_Euro = new EuropeanCallOption(165);
		StatCollector IBMEuropeanStat = new StatCollector();
		final long tmp1 = System.currentTimeMillis();
		do{
			IBMEuropeanStat.Update(IBM_Euro.getPayout(IBMStockPath));
			if (IBMEuropeanStat.GetN()%100000==0)
			{
				System.out.println("Turn["+String.valueOf(IBMEuropeanStat.GetN())+"]"
						+":  Future Payout:"  + String.valueOf(IBMEuropeanStat.GetPrice())+ ", Status report: "
						+	 String.valueOf(IBMEuropeanStat.Report()));
			}
		}
		while (IBMEuropeanStat.Report()>hurdleValue ||IBMEuropeanStat.GetN()<100 );
		// minimum number of 100 runs here is to avoid random disruptions caused by fake convergence

		double price = IBMEuropeanStat.GetPrice();
		double discountedprice=price*Math.exp(-rate*N);
		System.out.println("IBM European Option price is " + String.valueOf(discountedprice));
		System.out.println("Took "+String.valueOf(IBMEuropeanStat.GetN()) +" iterations");
		long milliseconds = System.currentTimeMillis()-tmp1;
		int seconds = (int) (milliseconds / 1000) % 60 ;
		int minutes = (int) ((milliseconds / (1000*60)) % 60);
		int hours   = (int) ((milliseconds / (1000*60*60)) % 24);
		System.out.println("Pricing took "+hours+" hours "+minutes+" minutes "+ seconds+" seconds.");

		// price IBM Asian option
		AsianCallOption IBM_Asian = new AsianCallOption(164);
		StatCollector IBMAsianStat = new StatCollector();
		System.out.println("IBM Asian Option: Pricing....");
		final long tmp2 = System.currentTimeMillis();
		do{
			IBMAsianStat.Update(IBM_Asian.getPayout(IBMStockPath));
			if (IBMAsianStat.GetN()%100000==0)
			{
				System.out.println("Turn["+String.valueOf(IBMAsianStat.GetN())+"]"
						+":  Future Payout:"  + String.valueOf(IBMAsianStat.GetPrice())+ ", Status report: "
						+	 String.valueOf(IBMAsianStat.Report()));
			}
		}
		while (IBMAsianStat.Report()>hurdleValue ||IBMAsianStat.GetN()<100 );
		// minimum number of 100 runs here is to avoid random disruptions caused by fake convergence

		double asianprice = IBMAsianStat.GetPrice();
		double discountedasianprice=asianprice*Math.exp(-rate*N);
		System.out.println("IBM Asian Option price is " + String.valueOf(discountedasianprice));
		System.out.println("Took "+String.valueOf(IBMAsianStat.GetN())+" iterations");
		long milliseconds2 = System.currentTimeMillis()-tmp2;
		int seconds2 = (int) (milliseconds2 / 1000) % 60 ;
		int minutes2 = (int) ((milliseconds2 / (1000*60)) % 60);
		int hours2   = (int) ((milliseconds2 / (1000*60*60)) % 24);
		System.out.println("Pricing took "+hours2+" hours "+minutes2+" minutes "+ seconds2+" seconds.");
	}
}
