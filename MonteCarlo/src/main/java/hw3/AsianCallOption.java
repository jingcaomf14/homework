package hw3;

import java.util.List;

import org.joda.time.DateTime;

public class AsianCallOption implements PayOut {
	
	private double K; // K is the strike price
	
	public AsianCallOption(double K){
		this.K = K;
	}

	@Override
	public double getPayout(StockPath path) {
		List<Pair<DateTime,Double>> trajectory = path.getPrice();
		double[] prices = new double[trajectory.size()];
		double averageprice = 0;
		double sum =0;
		// transfer price trajectory to an array of doubles named prices
		for (int i=0; i<trajectory.size(); i++)
		{
			prices[i]=trajectory.get(i).getValue();
		}
		
		// calculate average price of the price trajectory to prepare for Asian option payout pricing
		for (int i=0; i<trajectory.size(); i++)
		{
			sum=sum + trajectory.get(i).getValue();
		}
		averageprice = sum / (double) trajectory.size();
		
		return Math.max(0, averageprice - K);
	}

}
