package hw3;

import junit.framework.TestCase;

public class Test_NormalVectorGenerator extends TestCase {

	protected void setUp(){	}
	
	protected void tearDown(){}
	
	// test whether NormalVectorGenerator produces a normal random array of size N
	// print 10 experiments to generate normal random vector of size 5
	
	public void testGeneratorSize()
	{
		NormalVectorGenerator testgen = new NormalVectorGenerator(5);
		double[] testvector = testgen.getVector();
		assertTrue(testvector.length==5);
		for (int i=0; i<10; i++)
		{
		testvector = testgen.getVector();
		System.out.println("Test "+ String.valueOf(i+1)+ ": Printing a normal random vector of size 5");
			for(int j=0; j<5;j++)
			{
			System.out.print(String.valueOf(testvector[j])+" ");
			}
		System.out.println();
		}
	}
	
}
