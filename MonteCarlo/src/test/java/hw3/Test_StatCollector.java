package hw3;

import junit.framework.TestCase;

public class Test_StatCollector extends TestCase {
	protected void setUp(){	}
	
	protected void tearDown(){	}
	
	public static void testStatColletcor(){
		StatCollector test = new StatCollector();
		double[] testpayoutarray = new double[] {3,4,5,6,7,8,9};
		double testtrueaverage = 6;
		double testtrueaveragesquare=40;
		double testtruesigma= 2;
		
		for(int i=0; i<7; i++)
		{
			test.Update(testpayoutarray[i]);
		}
		assertTrue(test.payOutAverage==testtrueaverage);
		assertTrue(test.payOutSquaredAverage==testtrueaveragesquare);
		assertTrue(test.sigma==testtruesigma);
	}
}
