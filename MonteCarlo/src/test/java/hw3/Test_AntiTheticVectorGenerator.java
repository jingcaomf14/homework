package hw3;

import junit.framework.TestCase;

public class Test_AntiTheticVectorGenerator extends TestCase {
	protected void setUp(){}
	
	protected void tearDown(){}
	
	// test whether the decorator produces an array of double the original size
	// print 10 experiments to generate normal random vector of size 5,
	// which are supposed to be in reverse mirror of each other
		
		public void testAntiTheticGeneratorSize()
		{
			NormalVectorGenerator testgen = new NormalVectorGenerator(5);
			AntiTheticVectorGenerator testantigen = new AntiTheticVectorGenerator(testgen);
			for (int i=0;  i<2; i++)
			{
			double[] testvector = testantigen.getVector();
			assertTrue(testvector.length==5);
			}
			
			for (int i=0; i<10; i++)
			{
			double[] testvector = testantigen.getVector();
			System.out.println("Test "+ String.valueOf(i+1)+ ": Printing a normal random vector of size 5");
				for(int j=0; j<5;j++)
				{
				System.out.print(String.valueOf(testvector[j])+" ");
				}
			System.out.println();
			}
		}
}
