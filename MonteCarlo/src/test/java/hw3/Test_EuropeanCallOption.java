package hw3;

import org.joda.time.DateTime;

import junit.framework.TestCase;

public class Test_EuropeanCallOption extends TestCase {
	protected void setUp(){	}
	
	protected void tearDown(){ }
	
	public void testEuropeanPayout(){
		double rate = 0;
		int N = 5;
		double S0= 100;
		double sigma = 0;
		DateTime startDate = DateTime.now();
		DateTime endDate= startDate.plusDays(N);
		int k =150;
		
	// fake a deterministic path by setting rate and volatility of the stock as 0.
		NormalVectorGenerator normvecgen =  new NormalVectorGenerator(5);
		GBMRandomStockPathGenerator testpathgen=new GBMRandomStockPathGenerator( rate,  N, sigma, S0, startDate, endDate,normvecgen);
		// if k is larger than 100, payout will always be 0, and if k is less than 100, payout will always be 100-k
		k=(int)Math.random()*200;
		EuropeanCallOption testoption = new EuropeanCallOption(k);
		double testpayout = testoption.getPayout(testpathgen);
		assertTrue(testpayout==Math.max(0,100-k));
		
	}
	
}