package hw3;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

import junit.framework.TestCase;

public class Test_GBMRandomStockPathGenerator extends TestCase {
	
	protected void setUp(){}
	
	protected void tearDown(){}
	
	double rate = 0.1;
	int N = 5;
	double S0= 100;
	double sigma = 0.2;
	DateTime startDate = DateTime.now();
	DateTime endDate= startDate.plusDays(N);
	
	
	public void testStockPathGenerator()
	{
		// first use NormalVectorGenerator to make a random vector of size 5
		NormalVectorGenerator normvecgen =  new NormalVectorGenerator(5);
		GBMRandomStockPathGenerator testpathgen=new GBMRandomStockPathGenerator( rate,  N, sigma, S0, startDate, endDate,normvecgen);
		
		// test the size of the price variable
		assertTrue(testpathgen.getPrice().size()==5);
		
		// test that all prices are positive
		List<Pair<DateTime, Double>> testpath =new LinkedList<Pair<DateTime,Double>>();
		testpath= testpathgen.getPrice();
		double min = Integer.MAX_VALUE;
		for(int i=0; i<5; i++)
		{
		
			if(testpath.get(i).getValue()<min)
			{
				min =testpath.get(i).getValue(); 
			}
		}
		assertTrue(min>0);
		
		// print a list of sample stockpath
		
		for (int i=0; i<10; i++)
		{
			testpath= testpathgen.getPrice();
		System.out.println("Test "+ String.valueOf(i+1)+ ": Printing a stock path of size 5");
			for(int j=0; j<5;j++)
			{
			System.out.print(String.valueOf(testpath.get(j).getValue())+" ");
			}
		System.out.println();
		}
		
	}
	
	
}
