package hw3;

import java.util.LinkedList;
import java.util.List;

import org.joda.time.DateTime;

import junit.framework.TestCase;

public class Test_AsianCallOption extends TestCase {

	public void testEuropeanPayout(){
		double rate = 0.1;
		int N = 5;
		double S0= 100;
		double sigma = 0;
		DateTime startDate = DateTime.now();
		DateTime endDate= startDate.plusDays(N);
		int k =150;
		
	// fake a deterministic increasing path that ends at S0*exp(rN) by setting volatility of the stock as 0.
		NormalVectorGenerator normvecgen =  new NormalVectorGenerator(5);
		GBMRandomStockPathGenerator testpathgen=new GBMRandomStockPathGenerator( rate,  N, sigma, S0, startDate, endDate,normvecgen);
		// if k is larger than 100, payout will always be 0, and if k is less than 100, payout will always be 100-k
		k=(int)Math.random()*200;
		AsianCallOption testoption = new AsianCallOption(k);
		double testpayout = testoption.getPayout(testpathgen);
		List<Pair<DateTime, Double>> testpath =new LinkedList<Pair<DateTime,Double>>();
		testpath= testpathgen.getPrice();
		for(int j=0; j<5;j++)
		{
		System.out.print(String.valueOf(testpath.get(j).getValue())+" ");
		}
		System.out.println();
		double averagestockprice = 0;
		for (int i=0; i<5; i++)
		{averagestockprice=averagestockprice+S0*Math.exp(rate*i);}
		averagestockprice=(double)averagestockprice/5;
		
		assertTrue(Math.abs(testpayout-Math.max(0,averagestockprice-k))<0.0001);
		
	}
}
